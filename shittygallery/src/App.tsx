import { useState, useCallback } from 'react';
import { PictureDeck } from './components/PictureDeck';

const defaultPhotos = [
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048771354140763/PXL_20230203_221126012.jpg',
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048771148632095/PXL_20230209_022248567.jpg',
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048770880192553/PXL_20230222_014235584.jpg',
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048770611748864/PXL_20230225_170704526.jpg',
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048770393657494/PXL_20230311_021830569.jpg',
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048769877745704/PXL_20230401_233756975.jpg',
  'https://cdn.discordapp.com/attachments/996937908697182263/1094048769609322566/PXL_20230406_181134931.jpg'
]

function App() {
  const [photoDeck, setPhotoDeck] = useState(defaultPhotos);

  return (
    <div>
      <InputBox photoDeck={photoDeck} onPhotoDeckChange={setPhotoDeck} />
      <PictureDeck imageUrls={photoDeck}/>
    </div>
  )
}

type InputBoxProps = {
  onPhotoDeckChange: (deck: string[]) => void;
  photoDeck: string[];
}
const InputBox = (props: InputBoxProps) => {
  const { photoDeck, onPhotoDeckChange } = props;
  const [value, setValue] = useState<string | undefined>();


  const onAddImage = useCallback(() => {
    if (!value) {
      return;
    }
    const copySlice = photoDeck.slice();
    copySlice.push(value);
    onPhotoDeckChange(copySlice);
  }, [value, onPhotoDeckChange]);

  return (
    <div>
      <span>Url to an image</span>
      <input value={value} onChange={(e) => setValue(e.target.value)} />
      <button onClick={onAddImage}>Add Picture</button>
    </div>
  );
}

export default App
