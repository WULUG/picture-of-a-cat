import React, { useMemo } from 'react';
import * as Accordion from '@radix-ui/react-accordion';
import styled from 'styled-components';

type PictureDeckProps = {
  imageUrls: string[];
};

export const PictureDeck = (props: PictureDeckProps) => {
  const { imageUrls } = props;

  const photoCards: React.ReactNode[] = useMemo(() => {
    return imageUrls.map((url, idx) => <PictureCard key={`photo-card-${idx}`} value={`Photo ${idx}`} src={url} />);
  }, [imageUrls]);

  return (
    <StyledAccordionRoot
      type='single'
      collapsible
    >
      {photoCards}
    </StyledAccordionRoot>
  );
};

type PictureCardProps = {
  value: string;
  src: string;
}

const PictureCard = (props: PictureCardProps) => {
  return (
      <Accordion.Item value={props.value}>
        <StyledTrigger data-orientation={'horizontal'} >
          <StyledTriggerImage src={props.src} />
        </StyledTrigger>

        <StyledContent>
          <StyledImageCard src={props.src} />
        </StyledContent>
      </Accordion.Item>
  );
}

const StyledTrigger = styled(Accordion.Trigger)`
  background-color: transparent;
  height: 100px;
  width: 500px;
  padding: unset;
  border: unset;
  border-radius: unset;
  background-color: white;

  .button:focus {
    outline: unset;
  }
`;

const StyledContent = styled(Accordion.Content)`
  display: flex;
  justify-content: 'center';
`;

const StyledImageCard = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 5px;
  width: 500px;
  object-fit: scale-down;
  flex: 1;
`;

const StyledTriggerImage = styled.img`
  height: 100px;
  width: 500px;
  clip: rect(0, 500px, 100px, 0);
  object-fit: cover;
`;

const StyledAccordionRoot = styled(Accordion.Root)`
  width: 500px;
`;